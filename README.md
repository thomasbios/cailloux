<h1>Instalation</h1>

 `npm install`
`node index.js`

L'API est maintenant disponible sur le port `8080`.
<h1>Routes</h1>

`/material`

[GET] Liste tous les matériaux.

[POST] Ajoute un nouveau matériau.

body obligatoire :
```
{
	"name": String,
	"solidityIndex": Number,
	"sealingIndex": Number,
	"description": String
}
```
`/material/:name`

[PUT] Modifie un ou plusieurs attributs d'un matériau.

body (Chaque valeur est optionnelle):
```
{
	"solidityIndex": Number,
	"sealingIndex": Number,
	"description": String
}
```
[GET] Donne les détails d'un matériau.

[DELETE] Supprime un matériau.

`/type`

[GET] Liste tous les type de cailloux.

[POST] Ajoute un nouveau type.

body obligatoire :
```
{
	"name": String,
	"solidityIndex": Number,
	"sealingIndex": Number,
	"description": String
}
```
`/type/:name`

[PUT] Modifie un ou plusieurs attributs d'un type.

body (Chaque valeur est optionnelle):

```
{
	"solidityIndex": Number,
	"sealingIndex": Number,
	"description": String
}
```

[GET] Donne les détails d'un type.

[DELETE] Supprime un type.

`/stone`

[GET] Liste tous les cailloux.

[POST] Ajoute un nouveau caillou.

body obligatoire :
```
{
    "typeName": String,
    "materialName": String,
    "dimensions": {
        "length": Number,
        "width": Number,
        "height": Number
    },
    "condition": String,
    "color": String,
    "weight": Number,
    "description": String,
    "price": Number
}
```
`/stone/:id`

[PUT] Modifie un ou plusieurs attributs d'un caillou.

body (Chaque valeur est optionnelle):
```
{
    "typeName": String,
    "materialName": String,
    "dimensions": {
        "length": Number,
        "width": Number,
        "height": Number
    },
    "condition": String,
    "color": String,
    "weight": Number,
    "description": String,
    "price": Number
}
```
[GET] Donne les détails d'un caillou.

[DELETE] Supprime un caillou.


`/stone/stats/totalVolume`

[GET] Donne le volume total de tous les cailloux stockés.


`/stone/stats/totalWeight`

[GET] Donne le poids total additionné des cailloux stockés.

`/stone/type/:name`

[GET] Liste tous les cailloux du type spécifié.

`/stone/material/:name`

[GET] Liste tous les cailloux du materiau spécifié.