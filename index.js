let express = require('express');
let bodyParser = require('body-parser');

let app = express();
app.use(bodyParser.json());

// Material routes
let MaterialController = require('./material/material.controller');
app.route('/material')
    .get([
        MaterialController.getAll
    ])
    .post((req, res) => {
        MaterialController.insert(req, res);
    });

app.route('/material/:name')
    .put((req, res) => {
        MaterialController.change(req, res);
    })
    .get((req, res) => {
        MaterialController.getOne(req, res);
    })
    .delete((req, res) => {
        MaterialController.delete(req, res);
    });

// Type routes
let TypeController = require('./type/type.controller');
app.route('/type')
    .get([
        TypeController.getAll
    ])
    .post((req, res) => {
        TypeController.insert(req, res);
    });

app.route('/type/:name')
    .put((req, res) => {
        TypeController.change(req, res);
    })
    .get((req, res) => {
        TypeController.getOne(req, res);
    })
    .delete((req, res) => {
        TypeController.delete(req, res);
    });

// Stone routes
let StoneController = require('./stone/stone.controller');
app.route('/stone')
    .get([
        StoneController.getAll
    ])
    .post((req, res) => {
        StoneController.insert(req, res);
    });

app.route('/stone/:id')
    .put((req, res) => {
        StoneController.change(req, res);
    })
    .get((req, res) => {
        StoneController.getOne(req, res);
    })
    .delete((req, res) => {
        StoneController.delete(req, res);
    });

app.get('/stone/stats/totalVolume', [
    StoneController.getTotalVolume
]);

app.get('/stone/stats/totalWeight', [
    StoneController.getTotalWeight
]);

app.get('/stone/type/:name', [
    StoneController.getByType
]);

app.get('/stone/material/:name', [
    StoneController.getByMaterial
]);

app.listen(8080);