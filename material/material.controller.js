let modelClass = require('./material.model.js');
let materialService = require('./material.service.js');

exports.getAll = (req, res) => {
    let dataMaterials = materialService.get();
    let JSON = {
        status: "OK",
        materials: dataMaterials,
    };
    res.status(200).send(JSON);
};

exports.getOne = (req, res) => {
    let name = req.params.name;
    let dataMaterial = materialService.get(name);
    if (dataMaterial == null)
    {
        res.status(400).send(
        {
            status: "FAILURE",
            errorMessage: "Material not in the data base"
        });
    }
    else
    {
        res.status(200).send(
        {
            status: "OK",
            material: dataMaterial
        });
    }
};

exports.insert = (req, res) => {
    let data = req.body;
    model = new modelClass.MaterialModel(
        data.name,
        data.solidityIndex,
        data.sealingIndex,
        data.description
        // I don't know why the constructor does not 'construct'
    );
    model.name = data.name;
    model.solidityIndex = data.solidityIndex;
    model.sealingIndex = data.sealingIndex;
    model.description = data.description;

    if (materialService.insert(model))
        res.status(200).send(
        {
            status: "SUCCESS",
            material: model.toJSON()
        });
    else
        res.status(400).send(
        {
            status: "FAILURE",
            errorMessage: "Material name already in data base"
        });
};

exports.change = (req, res) => {
    let name = req.params.name;
    if (materialService.change(name, req.body))
        res.status(200).send(
        {
            status: "SUCCESS"
        });
    else
        res.status(400).send(
        {
            status: "FAILURE",
            errorMessage: "Material not in the data base"
        });
};

exports.delete = (req, res) => {
    let name = req.params.name;
    if (materialService.delete(name))
        res.status(200).send(
        {
            status: "SUCCESS"
        });
    else
        res.status(400).send(
        {
            status: "FAILURE",
            errorMessage: "Material not in the data base"
        });
};