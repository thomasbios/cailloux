let materials = [];

exports.insert = (materialModel) => {
    if (materials.some(e => e.name === materialModel.name))
        return false;
    materials.push(materialModel);
    return true;
};

exports.get = (name = null) => {
    if (name == null)
        return materials;
    return materials.find(e => e.name == name);
};

exports.change = (name, data) => {
    let finded = materials.find(e => e.name == name);
    if (finded == null)
        return false;
    if (data.solidityIndex != null)
        finded.solidityIndex = data.solidityIndex;
    if (data.sealingIndex != null)
        finded.sealingIndex = data.sealingIndex;
    if (data.description != null)
        finded.description = data.description;
    return true;
};

exports.delete = (name) => {
    if (this.get(name) == null)
        return false;
    materials = materials.filter(e => e.name != name);
    return true;
};