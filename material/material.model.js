class MaterialModel
{
    construtor(name, solidity, sealing, description)
    {
        this.name = name;
        this.solidityIndex = solidity;
        this.sealingIndex = sealing;
        this.description = description;
    };

    toJSON()
    {
        return {
            name: this.name,
            solidityIndex: this.solidityIndex,
            sealingIndex: this.sealingIndex,
            description: this.description
        };
    };
};

module.exports = {
    MaterialModel: MaterialModel,
}