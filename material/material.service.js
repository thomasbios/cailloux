let materialRepository = require('./material.repository.js');

exports.get = (name = null) => {
    if (name == null)
        return materialRepository.get();
    return materialRepository.get(name);
};

exports.insert = (materialModel) => {
    return materialRepository.insert(materialModel);
};

exports.change = (name, body) => {
    return materialRepository.change(name, body);
};

exports.delete = (name) => {
    return materialRepository.delete(name);
};