let modelClass = require('./type.model.js');
let typeService = require('./type.service.js');

exports.getAll = (req, res) => {
    let dataType = typeService.get();
    let JSON = {
        status: "OK",
        types: dataType,
    };
    res.status(200).send(JSON);
};

exports.getOne = (req, res) => {
    let name = req.params.name;
    let dataType = typeService.get(name);
    if (dataType == null)
    {
        res.status(400).send(
        {
            status: "FAILURE",
            errorMessage: "stone type not in the data base"
        });
    }
    else
    {
        res.status(200).send(
        {
            status: "OK",
            type: dataType
        });
    }
};

exports.insert = (req, res) => {
    let data = req.body;
    model = new modelClass.TypeModel(
        data.name,
        data.solidityIndex,
        data.sealingIndex,
        data.description
    );
    model.name = data.name;
    model.solidityIndex = data.solidityIndex;
    model.sealingIndex = data.sealingIndex;
    model.description = data.description;

    if (typeService.insert(model))
        res.status(200).send(
        {
            status: "SUCCESS",
            type: model.toJSON()
        });
    else
        res.status(400).send(
        {
            status: "FAILURE",
            errorMessage: "stone type already in data base"
        });
};

exports.change = (req, res) => {
    let name = req.params.name;
    if (typeService.change(name, req.body))
        res.status(200).send(
        {
            status: "SUCCESS"
        });
    else
        res.status(400).send(
        {
            status: "FAILURE",
            errorMessage: "stone type not in the data base"
        });
};

exports.delete = (req, res) => {
    let name = req.params.name;
    if (typeService.delete(name))
        res.status(200).send(
        {
            status: "SUCCESS"
        });
    else
        res.status(400).send(
        {
            status: "FAILURE",
            errorMessage: "stone type not in the data base"
        });
};