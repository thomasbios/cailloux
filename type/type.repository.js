let types = [];

exports.insert = (typeModel) => {
    if (types.some(e => e.name === typeModel.name))
        return false;
        types.push(typeModel);
    return true;
};

exports.get = (name = null) => {
    if (name == null)
        return types;
    return types.find(e => e.name == name);
}

exports.change = (name, data) => {
    let finded = types.find(e => e.name == name);
    if (finded == null)
        return false;
    if (data.solidityIndex != null)
        finded.solidityIndex = data.solidityIndex;
    if (data.sealingIndex != null)
        finded.sealingIndex = data.sealingIndex;
    if (data.description != null)
        finded.description = data.description;
    return true;
}

exports.delete = (name) => {
    if (this.get(name) == null)
        return false;
    types = types.filter(e => e.name != name);
    return true;
}