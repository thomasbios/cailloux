let typeRepository = require('./type.repository.js');

exports.get = (name = null) => {
    if (name == null)
        return typeRepository.get();
    return typeRepository.get(name);
};

exports.insert = (typeModel) => {
    return typeRepository.insert(typeModel);
};

exports.change = (name, body) => {
    return typeRepository.change(name, body);
};

exports.delete = (name) => {
    return typeRepository.delete(name);
};