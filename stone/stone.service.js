let stoneRepository = require('./stone.repository.js');
let stoneClass = require('./stone.model.js');

let materialRepository = require('../material/material.repository.js');
let typeRepository = require('../type/type.repository.js');


exports.get = (id = null) => {
    if (id == null)
        return stoneRepository.get();
    return stoneRepository.get(id);
};

exports.insert = (stoneModel, res) => {
    if (!typeRepository.get(stoneModel.typeName))
    {
        res.status(400).send(
        {
            status: "FAILURE",
            errorMessage: "The type does not exist"
        });
        return;
    }
    if (!materialRepository.get(stoneModel.materialName))
    {
        res.status(400).send(
        {
            status: "FAILURE",
            errorMessage: "The material does not exist"
        });
        return;
    }
    if (stoneRepository.insert(stoneModel))
        res.status(200).send(
        {
            status: "SUCCESS",
            data: stoneModel.toJSON()
        });
    else
        res.status(400).send(
        {
            status: "FAILURE",
        });
}

exports.change = (id, body) => {
    return stoneRepository.change(id, body);
};

exports.delete = (id) => {
    return stoneRepository.delete(id);
};

exports.getTotalVolume = () => {
    return stoneRepository.getTotalVolume();
};

exports.getTotalWeight = () => {
    return stoneRepository.getTotalWeight();
};

exports.getByType = (type, res) => {
    if (!typeRepository.get(type))
    {
        res.status(400).send(
        {
            status: "FAILURE",
            errorMessage: "The type does not exist"
        });
        return;
    }
    let data = stoneRepository.getByType(type);
    if (data == null)
        res.status(400).send(
        {
            status: "FAILURE",
        });
    else
        res.status(200).send(
        {
            status: "OK",
            data: data
        });
};

exports.getByMaterial = (mat, res) => {
    if (!materialRepository.get(mat))
    {
        res.status(400).send(
        {
            status: "FAILURE",
            errorMessage: "The material does not exist"
        });
        return;
    }
    let data = stoneRepository.getByMaterial(mat);
    if (data == null)
        res.status(400).send(
        {
            status: "FAILURE",
        });
    else
        res.status(200).send(
        {
            status: "OK",
            data: data
        });
};