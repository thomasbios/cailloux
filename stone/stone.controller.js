let stoneClass = require('./stone.model.js');
let stoneService = require('./stone.service.js');

exports.getAll = (req, res) => {
    let dataStone = stoneService.get();
    let JSON = {
        status: "OK",
        stones: dataStone,
    };
    res.status(200).send(JSON);
};

exports.insert = (req, res) => {
    let data = req.body;
    model = new stoneClass.StoneModel(data);
    // I don't know why the constructor does not 'construct'
    model.typeName = data.typeName;
    model.materialName = data.materialName;
    model.dimensions = {
        lenght: data.dimensions.length,
        width: data.dimensions.width,
        height: data.dimensions.height
    };
    model.condition = data.condition;
    model.color = data.color;
    model.weight = data.weight;
    model.description = data.description;
    model.price = data.price;

    stoneService.insert(model, res);
};

exports.getOne = (req, res) => {
    let id = req.params.id;
    let data = stoneService.get(id);
    if (data == null)
        res.status(400).send(
        {
            status: "FAILURE",
            errorMessage: "Bad stone id"
        });
    else
        res.status(200).send(
        {
            status: "OK",
            stone: data
        });
};

exports.change = (req, res) => {
    let id = req.params.id;
    if (stoneService.change(id, req.body))
        res.status(200).send(
        {
            status: "SUCCESS"
        });
    else
        res.status(400).send(
        {
            status: "FAILURE",
            errorMessage: "Bad stone id"
        });
};

exports.delete = (req, res) => {
    let id = req.params.id;
    if (stoneService.delete(id))
        res.status(200).send(
        {
            status: "SUCCESS"
        });
    else
        res.status(400).send(
        {
            status: "FAILURE",
            errorMessage: "Bad stone id"
        });
};

exports.getTotalVolume = (req, res) => {
    let volume = stoneService.getTotalVolume();
    res.status(200).send(
        {
            status: "SUCCESS",
            totalVolume: volume
        });
};

exports.getTotalWeight = (req, res) => {
    let weight = stoneService.getTotalWeight();
    res.status(200).send(
        {
            status: "SUCCESS",
            totalWeight: weight
        });
};

exports.getByType = (req, res) => {
    let type = req.params.name;
    stoneService.getByType(type, res);
};

exports.getByMaterial = (req, res) => {
    let mat = req.params.name;
    stoneService.getByMaterial(mat, res);
}