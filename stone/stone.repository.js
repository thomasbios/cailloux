let stones = [];
let id = 0;

exports.get = (id = null) => {
    if (id == null)
        return stones;
    return stones.find(e => e.id == id);
};

exports.insert = (stoneModel) => {
    stoneModel.id = id;
    id++;
    stones.push(stoneModel);
    return true;
};

exports.change = (id, data) => {
    let finded = stones.find(e => e.id == id);
    if (finded == null)
        return false;
    if (data.typeName != null)
        finded.typeName = data.typeName;
    if (data.materialName != null)
        finded.materialName = data.materialName;
    if (data.dimensions != null)
        finded.dimensions = data.dimensions;
    if (data.condition != null)
        finded.condition = data.condition;
    if (data.color != null)
        finded.color = data.color;
    if (data.weight != null)
        finded.weight = data.weight;
    if (data.description != null)
        finded.description = data.description;
    if (data.price != null)
        finded.price = data.price;
    return true;
};

exports.delete = (id) => {
    if (this.get(id) == null)
        return false;
    stones = stones.filter(e => e.id != id);
    return true;
}

exports.getTotalVolume = () => {
    let rtn = 0;
    stones.forEach(e => {
        rtn += (e.dimensions.lenght * e.dimensions.width * e.dimensions.height);
    });
    return rtn;
}

exports.getTotalWeight = () => {
    let rtn = 0;
    stones.forEach(e => {
        rtn += e.weight;
    });
    return rtn;
}

exports.getByType = (type) =>
{
    return stones.filter(e => e.typeName == type);
}

exports.getByMaterial = (mat) =>
{
    return stones.filter(e => e.materialName == mat);
}