class StoneModel
{
    construtor(data)
    {
        // I don't know why the constructor dos not 'construct'
        this.id = 0;
        this.typeName = data.typeName;
        this.materialName = data.materialName;
        this.dimensions.length = data.dimensions.length;
        this.dimensions.width = data.dimensions.width;
        this.dimensions.height = data.dimensions.height;
        this.condition = data.condition;
        this.color = data.color;
        this.weight = data.weight;
        this.description = data.description;
        this.price = data.price;
    };

    toJSON()
    {
        return {
            id: this.id,
            typeName: this.typeName,
            materialName: this.materialName,
            dimensions: this.dimensions,
            condition: this.condition,
            color: this.color,
            weight: this.weight,
            description: this.description,
            price: this.price
        };
    };
};

module.exports = {
    StoneModel: StoneModel,
}